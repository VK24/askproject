# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import DataMigration
from django.db import models

import random

class Migration(DataMigration):

    def forwards(self, orm):
        "Write your forwards methods here."
        # Note: Don't use "from appname.models import ModelName". 
        # Use orm.ModelName to refer to models in this application,
        # and orm['appname.ModelName'] for models in other applications.
        
        word_parts = ['as', 'sa', 're', 'er', 'qwe', 'ewq', 'ty', 'yt', 'ut', 'itu', 'op', 'apo', 'uri', 'tu', 'lo', 'li',
                      'gi', 'zu', 'zo', 'za', 'na', 'it', 'is', 'im']
        us = []
        for ii in xrange(50):
            user=orm.MyUser.objects.create(first_name=''.join([random.choice(word_parts) for i in xrange(2, random.randint(2, 5))]),
                         last_name=''.join([random.choice(word_parts) for i in xrange(2, random.randint(3, 6))]),
                         username=''.join([random.choice(word_parts) for i in xrange(2, random.randint(1, 4))]),
                         email=''.join([random.choice(word_parts) for i in xrange(2, random.randint(1, 3))]) + '@ili.net',
                         password='abc')
            for j in xrange(2):
                question = orm.Question.objects.create(author=user,
                                 header=" ".join(["".join([random.choice(word_parts) for i in xrange(2, random.randint(2, 5))]) for i in xrange(2, random.randint(2, 5))]),
                                 text=" ".join(["".join([random.choice(word_parts) for i in xrange(2, random.randint(2, 5))]) for i in xrange(2, random.randint(12, 45))]))
                
                for k in xrange(3):
                    orm.Answer.objects.create(question=question,
                                              author=random.choice(orm.User.objects.all()),
                                              text=" ".join(["".join([random.choice(word_parts) for i in xrange(2, random.randint(2, 5))]) for i in xrange(2, random.randint(12, 45))]))


    def backwards(self, orm):
        "Write your backwards methods here."
        raise RuntimeError('this migration cannot be reversed')

    models = {
        u'askproj.answer': {
            'Meta': {'object_name': 'Answer'},
            'author': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['askproj.User']"}),
            'creation_date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 6, 25, 0, 0)'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'question': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['askproj.Question']"}),
            'text': ('django.db.models.fields.TextField', [], {})
        },
        u'askproj.question': {
            'Meta': {'object_name': 'Question'},
            'author': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['askproj.User']"}),
            'creation_date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 6, 25, 0, 0)'}),
            'header': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'text': ('django.db.models.fields.TextField', [], {})
        },
        u'askproj.user': {
            'Meta': {'object_name': 'User'},
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '80'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '80'}),
            'nickname': ('django.db.models.fields.CharField', [], {'max_length': '80'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '80'}),
            'reg_data': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'})
        }
    }

    complete_apps = ['askproj']
    symmetrical = True
