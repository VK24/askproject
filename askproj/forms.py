from django import forms
from django.forms import ModelForm
from django.contrib.auth.forms import UserCreationForm
from django.contrib import auth

from askproj.models import MyUser, Question


class LoginForm(forms.Form):
    username = forms.CharField(label='Username')
    password = forms.CharField(label='Password', widget=forms.PasswordInput)

    def clean(self):
        cleaned_data = super(LoginForm, self).clean()
        if not self.errors:
            user = auth.authenticate(username=cleaned_data['username'], password=cleaned_data['password'])
            if user is None:
                raise forms.ValidationError('Wrong pair username/password')
            self.user = user
        return cleaned_data

    def get_user(self):
        return self.user or None 


class RegistrationForm(forms.Form):
    username = forms.CharField(label='User Name')
    email = forms.CharField(label='User Name', widget=forms.EmailInput)
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Confirm password', widget=forms.PasswordInput)

    def clean_username(self):
        username = self.cleaned_data.get('username')
        if MyUser.objects.filter(username=username).exists():
            raise forms.ValidationError('Username occupied. Please input another username')
        return self.cleaned_data

    def clean(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')
        if password1 != password2:
            raise forms.ValidationError('Passwords are different')
        return self.cleaned_data
        
class UserRegForm(UserCreationForm):
    class Meta:
        model = MyUser
        fields = ('username', 'first_name', 'last_name', 'email')
    #def save(self):
    #    username = MyUser.objects.create_user(username=self.
        

class QuestionForm(ModelForm):
    class Meta:
        model = Question
        fields = ['header', 'tags', 'text']


class AnswerForm(forms.Form):
    header = forms.CharField(label='Header')
