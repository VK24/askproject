# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import DataMigration
from django.db import models
from django.contrib.auth.hashers import make_password

import random

class Migration(DataMigration):

    def forwards(self, orm):
        "Write your forwards methods here."
        word_parts = ['as', 'sa', 're', 'er', 'qwe', 'ewq', 'ty', 'yt', 'ut', 'itu', 'op', 'apo', 'uri', 'tu', 'lo', 'li',
                      'gi', 'zu', 'zo', 'za', 'na', 'it', 'is', 'im', 'uj']
        tagname_list = ['food', 'relax', 'IT', 'nature', 'cars']
        tags = [orm.Tag.objects.create(tagname=tagname) for tagname in tagname_list]
        us = []
        qs = []
        for ii in xrange(random.randint(50, 70)):
            user = orm.MyUser.objects.create(
                first_name=''.join([random.choice(word_parts) for i in xrange(random.randint(2, 5))]),
                last_name=''.join([random.choice(word_parts) for i in xrange(random.randint(3, 6))]),
                username="%s%d" % (''.join([random.choice(word_parts) for i in xrange(random.randint(2, 4))]), ii),
                email=''.join([random.choice(word_parts) for i in xrange(random.randint(2, 3))]) + '@ili.net',
                password=make_password('abc'))
            us.append(user)
            for j in xrange(random.randint(2, 5)):
                question = orm.Question.objects.create(
                    author=user,
                    header=" ".join(["".join([random.choice(word_parts) for i in xrange(random.randint(2, 5))]) for i in xrange(random.randint(2, 5))]),
                    text=" ".join(["".join([random.choice(word_parts) for i in xrange(random.randint(2, 5))]) for i in xrange(random.randint(12, 45))]))
                question.tags = [random.choice(tags) for i in xrange(random.randint(1, 3))]
                question.save()
                qs.append(question)
                
        for question in qs:
            # Answers
            for k in xrange(random.randint(3, 5)):
                orm.Answer.objects.create(
                    question=question,
                    author=random.choice(us),
                    text=" ".join(["".join([random.choice(word_parts) for i in xrange(random.randint(2, 5))]) for i in xrange(random.randint(12, 45))]))
            # Like
            for user in us:
                if random.choice([True, False]):
                    orm.Like.objects.create(author=user, question = question)


    def backwards(self, orm):
        "Write your backwards methods here."
        raise RuntimeError('this migration cannot be reversed')

    models = {
        u'askproj.answer': {
            'Meta': {'object_name': 'Answer'},
            'author': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['askproj.MyUser']"}),
            'creation_date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 7, 8, 0, 0)'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'question': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['askproj.Question']"}),
            'text': ('django.db.models.fields.TextField', [], {})
        },
        u'askproj.like': {
            'Meta': {'object_name': 'Like'},
            'author': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['askproj.MyUser']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'question': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['askproj.Question']"})
        },
        u'askproj.myuser': {
            'Meta': {'object_name': 'MyUser', '_ormbases': [u'auth.User']},
            u'user_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['auth.User']", 'unique': 'True', 'primary_key': 'True'}),
            'views': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        u'askproj.question': {
            'Meta': {'object_name': 'Question'},
            'author': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['askproj.MyUser']"}),
            'creation_date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 7, 8, 0, 0)'}),
            'header': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'tags': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['askproj.Tag']", 'symmetrical': 'False'}),
            'text': ('django.db.models.fields.TextField', [], {}),
            'views': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        u'askproj.tag': {
            'Meta': {'object_name': 'Tag'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'tagname': ('django.db.models.fields.TextField', [], {})
        },
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['askproj']
    symmetrical = True
