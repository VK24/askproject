from django.db import models
from question import Question
from user import User
from django.utils import timezone
import datetime


class Answer(models.Model):
    question = models.ForeignKey(Question)
    author = models.ForeignKey(User)
    text = models.TextField()
    creation_date = models.DateTimeField(
        # default = timezone.local(timezone.now()) # datetime.datetime.now()
        default = datetime.datetime.now()
    )


