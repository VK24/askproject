from django.db import models
from user import User
from django.utils import timezone
import datetime


class Question(models.Model):
    author = models.ForeignKey(User)
    header = models.TextField()
    text = models.TextField()
    creation_date = models.DateTimeField(
        default = datetime.datetime.now()
        # default = timezone.local(timezone.now())
    )


