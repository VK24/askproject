from django.db import models
from django.utils import timezone
import datetime


class User(models.Model):
    first_name = models.CharField(max_length = 80)
    last_name = models.CharField(max_length = 80)
    nickname = models.CharField(max_length = 80) 
    email = models.EmailField()
    reg_data = models.DateTimeField(
        # default = timezone.local(timezone.now()) # datetime.datetime.now()
        default = datetime.datetime.now()
    )
    password = models.CharField(max_length = 80)

