from django.db import models

from django.contrib.auth.models import User
from django.utils import timezone
import datetime


class MyUser(User):
    views = models.IntegerField(default=0)
    def __unicode__(self):
        return '{0}'.format(self.username)

    def get_absolute_url(self):
        return '/proj/user/%d' % self.id

    def question_list(self):
        question_list = Question.objects.filter(author=self)
        return question_list

    def answer_list(self):
        answer_list = Answer.objects.filter(author=self)
        return answer_list


class Tag(models.Model):
    tagname = models.TextField()

    def get_absolute_url(self):
        return '/proj/tag/%d' % self.id

    def __unicode__(self):
        return "%s" % self.tagname


class Question(models.Model):
    author = models.ForeignKey(MyUser)
    header = models.TextField()
    text = models.TextField()
    views = models.IntegerField(default=0)
    tags = models.ManyToManyField(Tag)
    creation_date = models.DateTimeField(
        default = datetime.datetime.now()
        # default = timezone.local(timezone.now())
    )

    def __unicode__(self):
        return '%s' % self.header

    def answer_list(self):
        answer_list = Answer.objects.filter(question=self)
        return answer_list

    def get_absolute_url(self):
        return '/proj/question/%d' % self.id


class Like(models.Model):
    author = models.ForeignKey(MyUser)
    question = models.ForeignKey(Question)


class Answer(models.Model):
    question = models.ForeignKey(Question)
    author = models.ForeignKey(MyUser)
    text = models.TextField()
    creation_date = models.DateTimeField(
        # default = timezone.local(timezone.now()) # datetime.datetime.now()
        default = datetime.datetime.now()
    )

    def __unicode__(self):
        return '%s' % self.text

    def get_absolute_url(self):
        return self.question.get_absolute_url()

