from django.conf.urls import patterns, url
# from django.conf.urls.static import static

import views
from views import QuestionsView, UserView, QuestionDetailView

urlpatterns = patterns('',
    url(r'^hello$', views.hello),
    # url(r'^site$', views.site),
    # url(r'^question$', views.question),
    # url(r'^user$', views.user),
    # url(r'^user/(?P<pk>\d+)/$$', views.UserView.as_view()),
    url(r'^question/$', QuestionsView.as_view(), name='question list'),
    # url(r'^test$', views.question_list, name='question list'),
    # url(r'^question/(?P<pk>\d+)/$', QuestionDetailView.as_view()),
    url(r'^user/(?P<pk>\d+)/$$', views.user_view, name='user profile'),
    url(r'^question/(?P<pk>\d+)/$', views.question_view, name = 'question detail'),
    url(r'^tag/(?P<pk>\d+)/$', views.tag_view, name = 'tags questions'),
    url(r'^question/create/$', views.question_create, name = 'question create'),

    url(r'^login/$', views.login_lab, name = 'login'),
    url(r'^logout/$', views.logout, name = 'logout'),
    url(r'^registration/$', views.registration_view, name = 'registration'),
) # + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

