from django.http import HttpResponse
from django.shortcuts import render, render_to_response, redirect
from django.views.generic import ListView, DetailView
from models import *
from django.contrib import auth
from django.contrib.auth.forms import UserCreationForm
from django.core.context_processors import csrf
from django.template import RequestContext
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.exceptions import ObjectDoesNotExist

from forms import LoginForm, RegistrationForm, UserRegForm, QuestionForm, AnswerForm

def hello(request):
    return HttpResponse("Hello world!")


def site(request):
    return render(request, 'site.html')


def question(request):
    return render(request, 'question.html')


def user(request):
    return render(request, 'user.html')


class QuestionsView(ListView):
    model = Question
    paginate_by = 5


class UserView(DetailView):
    template_name = 'askproj/user.html'
    model = MyUser
    context_object_name = 'myuser'


def user_view(request, pk):
    context = RequestContext(request)
    user = MyUser.objects.get(id=pk)
    user.views = user.views + 1
    user.save()
    form = QuestionForm()
    context['form'] = form
    context['myuser'] = user
    return render_to_response('askproj/user.html', context)


def question_list(request):
    question_list = Question.objects.all()
    return render_to_response('askproj/question_list.html', {'object_list': question_list})


class QuestionDetailView(DetailView):
    template_name = 'askproj/question.html'
    model = Question


def question_create(request):
    if request.POST:
        header = request.POST.get('header', '')
        tag_names = request.POST.getlist('tags', '')
        print "Tags: ", tag_names
        content = request.POST.get('content', '')
        tags = Tag.objects.filter(id__in=tag_names)
        try:
            author = MyUser.objects.get(id=request.user.id)
        except ObjectDoesNotExist:
            return redirect('/proj/question/')
        question = Question(author=author, header=header, text=content)
        question.save()
        question.tags = tags
        question.save()
    return redirect('/proj/user/%d' % request.user.id)

def question_view(request, pk):
    question = Question.objects.select_related('myuser', 'answer', 'tag').get(id=pk)
    context = RequestContext(request)
    question.views = question.views + 1
    question.save()
    form = QuestionForm()
    context['form'] = form
    context['question'] = question
    return render_to_response('askproj/question.html', context)


def login(request):
    args = {}
    args.update(csrf(request))
    print "test"
    if request.POST:
        username = request.POST.get('username', '')
        password = request.POST.get('password', '')
        print "Username = %s" % username
        print "Password = %s" % password
        user = auth.authenticate(username=username, password=password)
        print "User = %s" % user
        if user is not None:
            auth.login(request, user)
            return redirect('/proj/question/')
        else:
            args['login_error'] = 'User not found' 
            return redirect('/proj/log_in/', args)

    else:
        return render_to_response('askproj/login.html', args)


def login_view(request):
    form = LoginForm(request)
    context = RequestContext(request, {'form': form})
    return render_to_response('askproj/login.html', context) 

def logout(request):
    auth.logout(request)
    return redirect('/proj/question/')


def login_lab(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            if form.get_user():
                auth.login(request, form.get_user())
                return redirect('/proj/question/')
    else:
        form = LoginForm()
    return render(request, 'askproj/login.html', {'form': form})

def registration(request):
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            user = MyUser.objects.create_user(username=form.username, email=form.email, password=form.password)
            auth.login(request, user)
            return redirect('/proj/question/')
    else:
        form = RegistrationForm()
    return render(request, 'askproj/register.html', {'form': form})


def registration_view(request):
    if request.method == 'POST':
        form = UserRegForm(request.POST)
        if form.is_valid():
            username = form.clean_username()
            password = form.clean_password2()
            form.save()
            new_user = auth.authenticate(username=username, password=password)
            auth.login(request, new_user)
            return redirect('/proj/question/')
    else:
        form = UserRegForm()
    return render(request, 'askproj/registration.html', {'form': form})


def tag_view(request, pk):
    context = RequestContext(request)
    tag = Tag.objects.select_related('question__tag').get(id=pk)
    all_question_list = tag.question_set.all()
    print "All questions: ", all_question_list
    paginator = Paginator(all_question_list, 5)
    print "Paginator ", paginator
    page = request.GET.get('page')
    try:
        question_list= paginator.page(page)
        print "Try"
    except PageNotAnInteger:
        print "Page is not an integer"
        # If page is not an integer, deliver first page.
        question_list= paginator.page(1)
        print "Small first list: ", question_list
    except EmptyPage:
        print "Empty page"
        # If page is out of range (e.g. 9999), deliver last page of results.
        question_list= paginator.page(paginator.num_pages)
    print "Small list: ", question_list

    # context['tag'] = tag
    context['question_list'] = question_list
    context['is_paginated'] = True
    return render_to_response('askproj/tag.html', context)


    

